curl $DOWNLOAD_URL -O render.blend

/usr/bin/blender-2.91.0-linux64/blender --background render.blend \
--render-frame $FRAME \
--render-output /blender/frames/frame_ \
--render-format PNG
--engine CYCLES \
--cycles-device CPU


frame_name=$(ls /blender/frames)

curl $UPLOAD_URL -F "frame=@$frame_name&number=$FRAME&id=$ID"