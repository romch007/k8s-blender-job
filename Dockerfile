FROM alpine

WORKDIR /blender

RUN apk update && apk add --no-cache curl
RUN mkdir -p /blender/frames

COPY blender2.91.tar.xz job.sh ./

RUN tar -xvf blender2.91.tar.xz -C /usr/bin/ && rm blender2.91.tar.xz

RUN chmod +x job.sh

CMD ["./job.sh"]